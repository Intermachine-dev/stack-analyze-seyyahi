import lyricsFinder from 'lyrics-finder'

export default async function handler(req, res) {
    const { artist, song } = req.query

    try {
        const lyric = await lyricsFinder(artist, song);
        res.status(200).json({ lyric })
    } catch (err) {
        res.status(400).json({ errMsg: err.message })
    }
}